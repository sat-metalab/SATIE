// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

// cartesianControl mapper defination: manipulate the position of the spatializer through the specification of 3D points in the Cartesian coordinate system.

~name = \cartesianControl;

~function = {
// arguments, corresponding to required spatializer parameters:
| aziDeg = 0, eleDeg = 0, gainDB = -99, delayMs = 1, lpHz = 15000, hpHz = 5, spread = 0.01, distance = 1, nfIndex = 0,

    coordinateX = 0, coordinateY = 0, coordinateZ = 0,

    refDistance = 1, maxDistance = 10000, rolloffFactor = 1|

    var cartesian3dPoint, newDistance, inverseDisModel;

    // Defines a point in 3D Cartesian coodiante.
    cartesian3dPoint = Cartesian.new(x: coordinateX, y: coordinateY, z: coordinateZ);

    aziDeg = cartesian3dPoint.theta.raddeg;
    eleDeg = cartesian3dPoint.phi.raddeg;

    // Calculate the modulo of the 3D point to obtain its linear distance from the origin.
    newDistance = cartesian3dPoint.rho;

    // newDistance = newDistance.clip(0.00001, maxDistance);

    // Implement the inverse distance model that calculates the gain based on the distance.
    inverseDisModel = refDistance / (refDistance + rolloffFactor * max(newDistance, refDistance) - refDistance);

    gainDB = (gainDB.dbamp * inverseDisModel).ampdb;

    [[aziDeg, eleDeg, gainDB, delayMs, lpHz, hpHz, spread, nfIndex]];

};
