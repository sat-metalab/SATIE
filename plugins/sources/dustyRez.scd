// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

/*  Audio plugin definition

	Each audio plugin should define the following global variables:
	~name: (symbol) name of the spatializer
	~description: (string) a short description
	~function: the actual definition of the plugin
*/

~name = \dustyRez;
~description = "Dust through resonators ";
~channelLayout = \mono;

~function = { |freq = 200, density = 10, attack=100, dec = 0.3, sus = 0.5, rel = 6, amp = 0.75, gate = 1|
	var env, envGen, dust, resonance;
	env = Env.adsr(attack, dec, sus, rel);
	envGen = EnvGen.kr(env, gate);
	dust = Dust.ar(density);
	resonance = DynKlank.ar(`[
		// frequency ratios
		[0.501, 1, 0.7,   2.002, 3, 9.6,   2.49, 11, 2.571,  3.05, 6.242, 12.49, 13, 16, 24],
		// amps
		[0.002,0.02,0.001, 0.008,0.02,0.004, 0.02,0.04,0.02, 0.005,0.05,0.05, 0.02, 0.03, 0.04],
		// ring times - "stutter" duplicates each entry threefold
		[0.2, 3.9, 2.25, 2.14, 1.07].stutter(3)],
		dust, freq);
	resonance = LeakDC.ar(resonance);
	resonance * envGen * amp
};

~setup = {};
