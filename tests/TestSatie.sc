TestSatie : SatieUnitTest {

	var satie, server;

	setUp {
		// even if we don't use the server, we still need to make a disposable one
		server = Server(this.class.name);
		satie = Satie(SatieConfiguration(server));
	}

	tearDown {
		// satie was not booted, so no need to quit
		server.remove;
	}

	test_satie_new {
		var result;
		// test whether Satie.new without arguments creates a Satie
		this.assertNoException({ result = Satie.new }, message: "Satie.new() should create a basic Satie");
		this.assertEquals(result.config.class, SatieConfiguration, "Satie.new should have created its own SatieConfiguration");
	}

	test_satie_debug_default {
		this.assertEquals(satie.debug, false, "Satie debug should be false by default");
	}

	test_satie_debug_setter_bool {
		satie.debug = true;
		this.assertEquals(satie.debug, true, "Satie debug setter should have set debug = true");
		satie.debug = false;
		this.assertEquals(satie.debug, false, "Satie debug setter should have set debug = false");
	}

	test_satie_debug_setter_nonBool {
		satie.debug = 0;
		[0, 1, \true, \false, "foo", $a].do { |i| satie.debug = i };
		this.assertEquals(satie.debug, false, "Trying to set a non-Boolean value should not effect Satie.debug");
	}

	test_satie_synthDescLib {
		this.assert(
			SynthDescLib.all.includesKey(\satie),
			"Satie created a new SynthDescLib named 'satie'"
		);
		this.assert(
			satie.synthDescLib.synthDescs.isEmpty,
			"Satie's SynthDescLib should initially be empty"
		);
		this.assert(
			satie.synthDescLib.servers.findMatch(satie.config.server).notNil,
			"Satie SynthDescLib's server is equal to Satie's server"
		);
	}

	test_loadSample_not_booted {
		var size = satie.audioSamples.size;
		satie.loadSample(\foobar, Platform.resourceDir +/+ "sounds/a11wlk01.wav");
		this.assertEquals(
			satie.audioSamples.size,
			size,
			"loadSample should not have any effect when the server is not booted"
		);
	}

	test_reconfigure_when_initialized {
		var outbus = 4;

		this.assertNoException(
			{ satie.reconfigure(SatieConfiguration(server: server, outBusIndex: [outbus])); },
			message: "Satie.reconfigure should not have thrown error when status was \\initialized"
		);

		this.assertEquals(
			satie.config.outBusIndex,
			[outbus],
			"Satie.reconfigure should have reconfigured Satie when status was \\initialized"
		);
	}

	test_reconfigure_incorrect_class {
		this.assertException(
			{ satie.reconfigure(Dictionary.new); },
			Error,
			"Satie.reconfigure should have thrown an error when not passed a SatieConfiguration"
		);
	}

	test_reconfigureFromJsonFile_valid {
		var path = this.class.filenameSymbol.asString.dirname +/+ "data" +/+ "valid.json";

		satie.reconfigureFromJsonFile(path);
		this.assertEquals(
			satie.config.listeningFormat,
			[\stereoListener, \domeVBAP],
			"Satie.reconfigureFromJsonString successfully reconfigured Satie"
		);
	}

	test_reconfigureFromJsonString_valid {
		var path = this.class.filenameSymbol.asString.dirname +/+ "data" +/+ "valid.json";
		var str = File.readAllString(path);

		satie.reconfigureFromJsonString(str);
		this.assertEquals(
			satie.config.listeningFormat,
			[\stereoListener, \domeVBAP],
			"Satie.reconfigureFromJsonString successfully reconfigured Satie"
		);
	}

	test_reconfigureFromJsonFile_invalid_syntax {
		var path = this.class.filenameSymbol.asString.dirname +/+ "data" +/+ "invalid.json";

		this.assertNoException(
			{ satie.reconfigureFromJsonFile(path); },
			"Satie.reconfigureFromJsonFile should not throw error when JSON syntax is invalid"
		);

		this.assertEquals(
			satie.config.listeningFormat,
			[\stereoListener],
			"Satie.reconfigureFromJsonFile should keep previous configuration when JSON syntax is invalid"
		);
	}

	test_reconfigureFromJsonString_invalid_syntax {
		var path = this.class.filenameSymbol.asString.dirname +/+ "data" +/+ "invalid.json";
		var str = File.readAllString(path);

		this.assertNoException(
			{ satie.reconfigureFromJsonString(str); },
			"Satie.reconfigureFromJsonString should not throw error when configuration JSON syntax is invalid"
		);

		this.assertEquals(
			satie.config.listeningFormat,
			[\stereoListener],
			"Satie.reconfigureFromJsonString should keep previous configuration when JSON syntax is invalid"
		);
	}

	test_reconfigureFromJsonFile_invalid_config {
		var path = "/file/does/not/exist";

		this.assertNoException(
			{ satie.reconfigureFromJsonFile(path); },
			"Satie.reconfigureFromJsonFile should not throw error when JSON configuration is invalid"
		);

		this.assertEquals(
			satie.config.listeningFormat,
			[\stereoListener],
			"Satie.reconfigureFromJsonFile should keep previous configuration when JSON configuration is invalid"
		);
	}

	test_reconfigureFromJsonString_invalid_config {
		var str = "this is not a json configuration string";

		this.assertNoException(
			{ satie.reconfigureFromJsonString(str); },
			"Satie.reconfigureFromJsonString should not throw error when configuration JSON is invalid"
		);

		this.assertEquals(
			satie.config.listeningFormat,
			[\stereoListener],
			"Satie.reconfigureFromJsonString should keep previous configuration when JSON configuration is invalid"
		);
	}

	test_enableHeartbeat_true {
		this.assertEquals(
			satie.osc.heartbeat,
			false,
			"Satie.osc.heartbeat should be false after Satie instantiation"
		);

		satie.enableHeartbeat(true);
		this.assertEquals(
			satie.osc.heartbeat,
			true,
			"satie.enableHeartbeat(true) should set SatieOSC.heartbeat to true"
		);
	}

	test_enableHeartbeat_not_boolean {
		satie.enableHeartbeat(\nonBool);
		this.assertEquals(
			satie.osc.heartbeat,
			false,
			"satie.enableHeartbeat should set SatieOSC.heartbeat to false when passed a non-boolean value"
		);
	}

	test_enabledHeatbeat_false {
		satie.enableHeartbeat(false);
		this.assertEquals(
			satie.osc.heartbeat,
			false,
			"satie.enableHeartbeat(false) should set SatieOSC.heartbeat to false"
		);
	}

}

