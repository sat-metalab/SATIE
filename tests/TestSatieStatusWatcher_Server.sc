TestSatieStatusWatcher_Server : SatieUnitTest {

	var satie, server, watcher;

	setUp {
		server = Server(this.class.name);
		satie = Satie(SatieConfiguration(server));

		// create a separate statusWatcher for testing
		// don't use the one inside the above Satie
		watcher = SatieStatusWatcher(satie);
	}

	tearDown {
		watcher.satieQuit;
		this.wait({ watcher.status === \initialized }, "% Satie failed to quit server".format(thisMethod), 3);
		server.remove;
	}

	test_watchServerBoot {
		watcher.satieBoot;
		this.wait({ watcher.status === \running }, "% Satie failed to boot".format(thisMethod), 30);
		this.assertEquals(
			watcher.status,
			\running,
			"watchServerBoot should have booted and set status to \\running"
		);
	}

	test_satieBoot_status_initialized {
		watcher.satieBoot;
		this.assertEquals(
			watcher.status,
			\booting,
			"Status should be set to \\booting immediately after calling satieBoot"
		);
		// wait for boot to finish
		this.wait({ watcher.status === \running }, "% Satie failed to boot".format(thisMethod), 30);
	}

	test_waitForSatieRunning_when_initialized {
		var timeout, cond = Condition.new;
		var result = false;

		watcher.waitForSatieRunning({ result = true; cond.unhang });
		timeout = fork { 10.wait; cond.unhang };
		cond.hang;
		timeout.stop;

		this.assertEquals(
			result,
			true,
			"waitForSatieRunning should have booted the server and evaluated onComplete when status \\initialized"
		);
	}

	test_waitForSatieRunning_when_booting {
		var timeout, cond = Condition.new;
		var result = false;

		watcher.satieBoot;
		watcher.waitForSatieRunning({ result = true; cond.unhang });
		timeout = fork { 10.wait; cond.unhang };
		cond.hang;
		timeout.stop;

		this.assertEquals(
			result,
			true,
			"waitForSatieRunning should have evaluated onComplete once booted when status \\booting"
		);
	}

	test_waitForSatieRunning_when_running {
		var timeout, cond = Condition.new;
		var result = false;

		// wait for Satie status \running
		watcher.satieBoot;
		this.wait({ watcher.status === \running }, "% Satie failed to boot".format(thisMethod), 30);

		// status already \running
		// should evaluate immediately
		watcher.waitForSatieRunning({ result = true; cond.unhang });
		timeout = fork { 10.wait; cond.unhang };
		cond.hang;
		timeout.stop;

		this.assertEquals(
			result,
			true,
			"waitForSatieRunning should evaluated onComplete immediately when status \\running"
		);
	}

	test_postServerBoot_action_registration {
		watcher.satieBoot;
		this.wait({ watcher.status === \running }, "% Satie failed to boot".format(thisMethod), 30);

		// check correct objects were added
		this.assert(CmdPeriod.objects.includes(satie), "Satie added to CmdPeriod");
		this.assert(ServerTree.objects.at(server).includes(satie), "Satie added to ServerTree");
		this.assert(ServerQuit.objects.at(server).includes(satie).not, "Satie was not added to ServerQuit");
		this.assert(ServerQuit.objects.at(server).includes(watcher), "SatieStatusWatcher was added to ServerQuit");
	}

	test_preQuit_action_deregistration {
		watcher.satieBoot;
		this.wait({ watcher.status === \running }, "% Satie failed to boot".format(thisMethod), 30);
		watcher.preQuit; // fake quitting

		// check correct objects were removed
		this.assert(CmdPeriod.objects.includes(satie).not, "Satie removed from CmdPeriod");
		this.assert(ServerTree.objects.at(server).includes(satie).not, "Satie removed from ServerTree");
		this.assert(ServerQuit.objects.at(server).includes(watcher).not, "SatieStatusWatcher removed from ServerQuit");
	}

}
