#!/bin/bash

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# global variables
LOG_PREFIX="SATIE-RES:"
SATIE_OPTIONS=" logprefix $LOG_PREFIX "
JACKD_RC="${HOME}/.jackdrc"
JACKD_PID="none"
JACKD_LAUNCH="y"
DEBUG_SATIE_SERVER="n"
SCLANG_PID="none"

# functions

function usage() {
echo "Usage: satie [OPTION]"
echo "This command runs a SATIE server."
echo
echo "Options:"
echo "  -l <format>    set listener format (default is stereoListener)"
echo "  -L             list listener formats"
echo "  -S             list sound object types"
echo "  -y             set scsynth instead of supernova"
echo "  -a <count>     set number of internal aux busses to create"
echo "  -g             display many debug lines in the terminal"
echo "  -h             print this help"
echo
echo "Monitoring options:"
echo "  -m             display SuperCollider meter"
echo "  -t             display SuperCollider tree"
echo "  -u             display basic spatialization UI (with -i option only)  "
echo "  -v             display volume controler  "
echo
echo "Audio device options:"
echo "  -i <#inputs>   set number of inputs (default is 0)"
echo "  -I <#index>    set input channel offset"
echo "  -n             no automatic generation of audio input spatialization"
echo "  -O <#index>    set output channel offset"
echo "  -b <size>      set SuperCollider blockSize"
echo "  -D             list audio devices (OSX only)"
echo "  -d <device>    use selected device (OSX only)"
echo "  -j <file>      use jack configuration file (default is ~/.jackdrc)"
echo "  -J             disable jack launching"
}

function on_exit() {
# killing jackd process
if [ $JACKD_LAUNCH = "y" ]
then
kill $JACKD_PID 2> /dev/null
fi
}

# option parsing
while getopts ":hmgtb:l:yLSi:j:JDd:uO:I:a:vn" opt; do
    case $opt in
	h)
	    usage >&2
	    exit 1
	    ;;
	m)
	    SATIE_OPTIONS+=" meter "
	    ;;
	n)
	    SATIE_OPTIONS+=" nosynths "
	    ;;
	g)
	    DEBUG_SATIE_SERVER="y"
	    ;;
	t)
	    SATIE_OPTIONS+=" tree "
	    ;;
	b)
	    SATIE_OPTIONS+=" blockSize $OPTARG "
	    ;;
	a)
	    SATIE_OPTIONS+=" auxBusCount $OPTARG "
	    ;;
	D)
	    SATIE_OPTIONS+=" scsynth "
	    SATIE_OPTIONS+=" listDevices "
	    ;;
	d)
	    TMP_OPTION=$(echo $OPTARG | tr " " %)
	    SATIE_OPTIONS+=" device $TMP_OPTION "
	    ;;
	l)
	    SATIE_OPTIONS+=" listener $OPTARG "
	    ;;
	v)
	    SATIE_OPTIONS+=" volumeCtl "
	    ;;
	y)
	    SATIE_OPTIONS+=" scsynth "
	    ;;
	L)
	    SATIE_OPTIONS+=" scsynth "
	    SATIE_OPTIONS+=" listListeners "
	    ;;
	S)
	    SATIE_OPTIONS+=" scsynth "
	    SATIE_OPTIONS+=" listSourceTypes "
	    ;;
	i)
	    SATIE_OPTIONS+=" numInputs $OPTARG "
	    ;;
	O)
	    SATIE_OPTIONS+=" outOffset $OPTARG "
	    ;;
	I)
	    SATIE_OPTIONS+=" inOffset $OPTARG "
	    ;;
	j)
	    JACKD_RC=$OPTARG
	    ;;
	J)
	    JACKD_LAUNCH="n"
	    ;;
	u)
	    SATIE_OPTIONS+=" inputUI "
	    ;;
	\?)
	    echo "Invalid option: -$OPTARG" >&2
	    ;;
	:)
	    echo "Option -$OPTARG requires an argument." >&2
	    exit 1
	    ;;
    esac
done

trap 'on_exit' INT
trap 'on_exit' KILL
trap 'on_exit' QUIT

# running jackd
if [ $JACKD_LAUNCH = "y" ]
then
JACKDARGS=$(cat $JACKD_RC | awk -F "jackd " '/1/ {print $2}')
jackd $JACKDARGS > /tmp/satie-jack.log 2> /tmp/satie-jack-err.log &
JACKD_PID=$(echo $!)
fi

# search for sclang
if [ x$SCLANG_BIN = "x" ] # check if SCLANG_BIN is a already defined
then
SCLANG_BIN=$(command -v sclang)
if [ x$SCLANG_BIN = "x" ] # check if SCLANG_BIN has been found with command
then
SCLANG_BIN="/Applications/SuperCollider.app/Contents/MacOS/sclang"
fi
fi

if [ ! -f ${SCLANG_BIN} ] # check if SCLANG_BIN is a file
then
echo "Supercollider interpreter (sclang) not found."
echo " You can set bash variable SCLANG_BIN in order to"
echo " specify a folder containing the sclang binary "
echo " Current content of SCLANG_BIN is: " $SCLANG_BIN
exit 1
fi


#running SATIE
SCLANG_PATH=$(echo ${SCLANG_BIN} | sed 's/sclang//')
if [ $DEBUG_SATIE_SERVER = "n" ]
then
${SCLANG_PATH}sclang ./satie.scd $SATIE_OPTIONS > /tmp/satie.log 2> /tmp/satie-err.log
else
${SCLANG_PATH}sclang ./satie.scd $SATIE_OPTIONS
fi

# print SATIE output
grep $LOG_PREFIX /tmp/satie.log | sed "s/$LOG_PREFIX//"

