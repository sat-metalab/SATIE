## How to make new releases
SATIE development uses a workflow inspired by _Gitflow_. Versioning follows the conventions of [SemVer](https://semver.org/), which states:

>>>
Given a version number MAJOR.MINOR.PATCH, increment the:

- MAJOR version when you make incompatible API changes,
- MINOR version when you add functionality in a backwards compatible manner, and
- PATCH version when you make backwards compatible bug fixes.
>>>

A new release of SATIE is planned **at most every 6 months**. The project maintainers can decide to release a new version whenever they deem necessary within that time.
This could be after important bugs fixes and new features, or when breaking changes are on the horizon.

The span a release period should be no more than 2-3 weeks in order to lessen the burden on maintainers.

### Steps to follow when releasing a new version
* Determine what the next `x.y.z` version number will be
* Create new branch off of `develop` called `x.y.z` using Gitlab's web interface
* Set the `x.y.z` branch as **protected** in Gitlab
* Merge MRs intended for version `x.y.z` into the `x.y.z` branch
* Occasionally, as needed, merge branch `x.y.z` into `develop`
    - If there are merge conflicts, **do not** resolve them using Gitlab's web interface:
         - `git checkout x.y.z && git pull origin x.y.z`
         - `git checkout develop && git pull origin develop`
         - `git merge x.y.z`
         - _resolve conflicts_
         - `git push origin develop`
         - _create MR on Gitlab_
* Increase the _version_ string in the _SATIE.quark_ file
* If needed, update the SC-HOA commit _dependency_ in the _SATIE.quark_ file
* Add a new entry in _NEWS.md_ with the version number, date, and a description of changes
* Create a MR to merge `x.y.z` into `master` using Gitlab's web interface
* Create a tag named `x.y.z` on the `master` branch
    - Tags can be created on Gitlab or locally. The local commands are:
        - `git checkout master`
        - `git pull origin master`
        - `git tag -a -m "Release of version x.y.z" x.y.z`
        - `git push --follow-tags origin master`
* Create a MR to merge `master` into `develop` using Gitlab's web interface
* Removed protected branch status and delete `x.y.z` using Gitlab's web interface

### Optional steps
If a release includes changes to the OSC protocol, document the changes to the OSC API in [./SATIE-OSC-API.md](./SATIE-OSC-API.md).

Make sure you have `pandoc` installed:

`sudo apt install pandoc`

Then, run the following command from the root of the SATIE repository:

`pandoc -s --toc -c doc.css -f markdown -t html SATIE-OSC-API.md -o HelpSource/Overview/OSC-API.html`
