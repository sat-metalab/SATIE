// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

/*
* SATIE for SuperCollider3
*
*/
Satie {
	var <execFile;
	var <config;
	var <>spat;
	var <debug = false;

	var <mastering;

	/*    RENDERER     */
	// buses
	var <auxbus;
	var <aux;
	// compiled definitions
	var <generators, <effects, <processes;
	// instantiated
	var <groups, <groupInstances, <processInstances;
	// id associations: Synth.nodeID -> instance.name
	var <namesIds;
	// OSC
	var <osc;
	// sample buffer dictionary
	var <audioSamples;

	// introspection
	const <synthDescLibName = \satie;
	var <inspector;
	var <synthDescLib;

	// mastering spatialisation. one unique synth is generater per spatializer
	var <postProcStruct;
	var <ambiPostProcStruct;
	var <postProcessors;
	var postProcGroup;
	var <ambiPostProcessors;
	var ambiPostProcGroup;
	var outputSynth;
	var hasOutputSynth = false;
	var statusWatcher;

	*new { |satieConfiguration, execFile|
		^super.newCopyArgs(execFile)
			.initNew(
				satieConfiguration ?? { SatieConfiguration.new }
			);
	}

	initNew { |configuration|
		this.initConfig(configuration);
		osc = SatieOSC(this);
		statusWatcher = SatieStatusWatcher(this);
		synthDescLib = SynthDescLib(synthDescLibName, config.server);
		inspector = SatieIntrospection(this);

		this.init;
	}

	initConfig { |configuration|
		if(configuration.class !== SatieConfiguration) {
			Error(
				"%: Invald argument 'configuration'. Received a %, expected a SatieConfiguration"
					.format(thisMethod, configuration.class)
			).throw;
		};

		config = configuration;
	}

	init { |configuration|
		// only set config when init is called with a configuration
		// otherwise, keep the current config
		if(configuration.notNil) {
			this.initConfig(configuration);
		};

		postProcStruct = Dictionary[
			\pipeline -> #[],
			\outputIndex -> 0,
			\spatializerNumber -> 0,
			\synthArgs -> #[]
		];
		ambiPostProcStruct = Dictionary[
			\pipeline -> #[],
			\order -> 1,
			\outputIndex -> 0,
			\spatializerNumber -> 0,
			\synthArgs -> #[]
		];
		postProcessors = Dictionary[];
		ambiPostProcessors = Dictionary[];
		groups = Dictionary[];
		groupInstances = Dictionary[];
		processInstances = Dictionary[];
		generators = IdentityDictionary[];
		effects = IdentityDictionary[];
		processes = Dictionary[];
		mastering = Dictionary[];
		namesIds = Dictionary[];
		audioSamples = IdentityDictionary[];

		osc.initOSC;
		synthDescLib.synthDescs.clear;
	}

	debug_ { |bool = false|
		debug = switch(bool)
			{true} {true}
			{false} {false}
			{debug};
	}

	status {
		^statusWatcher.status
	}

	booted {
		// for backwards compatibility
		^statusWatcher.booted
	}

	boot {
		statusWatcher.satieBoot;
	}

	reboot {
		statusWatcher.satieReboot;
	}

	reconfigure { |configuration|
		if(configuration.class !== SatieConfiguration) {
			Error(
				"%: Invald argument 'configuration'. Received a %, expected a SatieConfiguration"
					.format(thisMethod, configuration.class)
			).throw;
		};

		switch(this.status)
			{\initialized} { this.init(configuration); }
			{\running} {
				this.init(configuration);
				this.reboot;
			}
			{
				Error(
					"%: Unable to reconfigure. Satie status was %"
						.format(thisMethod, this.status)
				).throw;
			};
	}

	reconfigureFromJsonString { |string|
		var config;

		try {
			config = SatieConfiguration.fromJsonString(string);
		} { |e|
			e.errorString.error;
		};

		if(config.notNil) {
			^this.reconfigure(config);
		};
	}

	reconfigureFromJsonFile { |path|
		var config;

		try {
			config = SatieConfiguration.fromJsonFile(path);
		} { |e|
			e.errorString.error;
		};

		if(config.notNil) {
			this.reconfigure(config);
		};
	}

	quit { |quitServer = true|
		statusWatcher.satieQuit(quitServer);
	}

	waitForBoot { |onComplete|
		statusWatcher.waitForSatieRunning(onComplete);
	}

	// called by statusWatcher
	postBootExec {
		// execute any code needed after the server has been booted
		this.createDefaultGroups;
		this.loadBuffers;
		this.setAuxBusses;
		if(config.ambiOrders.notEmpty) { this.setAmbiBusses };
		if(config.listeningFormat.notEmpty) {

			// execute setup functions for spatializers
			config.listeningFormat.do { |item|
				var spatializer = config.spatializers[item.asSymbol];
				if(spatializer.setup.isNil) {
					if(debug) { "Spatializer% - no setup here".format(spatializer.name).postln };
				} {
					spatializer.setup.value(this);
				}
			};
		};

		config.server.sync;
		this.setupPlugins;
		if (config.generateSynthdefs, {
			this.makePlugins;
		});

		// create OSC handlers
		osc.createHandlers;

		config.server.sync;
		if(execFile.notNil) {
			"- Executing %".format(execFile).postln;
			this.executeExternalFile(execFile);
		};
	}

	setPostProcStruct {|pipeline, outputIndex = 0, spatializerNumber = 0, synthArgs = #[]|
		postProcStruct.put(\pipeline, pipeline);
		postProcStruct.put(\outputIndex, outputIndex);
		postProcStruct.put(\spatializerNumber, spatializerNumber);
		postProcStruct.put(\synthArgs, synthArgs);
	}

	setAmbiPostProcStruct {|pipeline, order = 1, outputIndex = 0, spatializerNumber = 0, synthArgs = #[]|
		ambiPostProcStruct.put(\pipeline, pipeline);
		ambiPostProcStruct.put(\order, order);
		ambiPostProcStruct.put(\outputIndex, outputIndex);
		ambiPostProcStruct.put(\spatializerNumber, spatializerNumber);
		ambiPostProcStruct.put(\synthArgs, synthArgs);
	}

	executeExternalFile { |filepath|

		// check that SATIE is running
		if(statusWatcher.status !== \running) {
			"%: Satie not booted. Cannot load file '%'.".format(thisMethod, filepath).error;
			^nil
		};

		if(File.existsCaseSensitive(filepath).not) {
			"%: file '%' not found".format(thisMethod, filepath).error;
			^nil
		};

		if(filepath.splitext.last != "scd", {
			"%: file '%' must be of type '.scd'".format(thisMethod, filepath).error;
			^nil
		});

		// load file and execute its code
		// this method will return once all lines of code in the file have been executed
		// there is no guarantee that any asynchronous commands have finished
		// Buffers and such may still be loading when executeExternalFile returns
		filepath.load;
	}

	clearBuffers {
		audioSamples.keysValuesDo { |key, buf| buf.free };
		audioSamples = IdentityDictionary[];
	}

	clearScene {
		this.cleanSlate;
		this.createDefaultGroups;
		if(hasOutputSynth) { this.createOutputSynth };
	}

	doOnServerTree {
		"SATIE - creating default groups".postln;
		this.createDefaultGroups;
	}

	cmdPeriod {
		"SATIE - clearing the scene".postln;
		this.cleanSlate;
	}

	clearOSC {
		osc.free;
	}

	enableHeartbeat { |bool = true|
		osc.heartbeat_(bool);
	}

	disableOSC {
		this.enableOSC(false)
	}

	enableOSC { |bool = true|
		var oscDefs = this.osc.oscDefs;
		if (bool) {
			oscDefs.do { |i| i.enable }
		} {
			oscDefs.do { |i| i.disable }
		}
	}

	createDefaultGroups {
		this.makeSatieGroup(\default, \addToHead);
		this.makeSatieGroup(\defaultFx, \addToTail);
		this.makePostProcGroup();
	}

	replacePostProcessor { |pipeline, outputIndex = 0, spatializerNumber = 0, synthArgs = #[]|

		if(statusWatcher.status !== \running) {
			"%: SATIE is not booted".format(thisMethod).warn
			^this
		};

		fork {
			var postprocname = ("post_proc_" ++ spatializerNumber).asSymbol;
			SynthDef(postprocname, { |synth_gate = 1|
				var xfade = EnvGen.kr(
					Env.asr(0.3, 1.0, 0.3, curve: \sine),
					gate: synth_gate,
					doneAction: Done.freeSelf
				);
				var previousSynth = SynthDef.wrap({
					In.ar(
						bus: config.outBusIndex[spatializerNumber],
						numChannels: config.spatializers[config.listeningFormat[spatializerNumber]].numChannels
					)
				});
				// collecting spatializers
				pipeline.do { |item|
					item = item.asSymbol;
					previousSynth = SynthDef.wrap(
						func: config.postprocessors[item].function,
						prependArgs: [previousSynth]
					);
					// add individual pipeline item to the dictionaries used by introspection
					groupInstances[\postProc].put(item, previousSynth);
					mastering.put(item, item);
				};
				XOut.ar(outputIndex, xfade, previousSynth);
			}).add;

			config.server.sync;
			postProcessors[postprocname].set(\synth_gate, 0);
			postProcessors.put(postprocname, Synth(postprocname, args: synthArgs, target: postProcGroup));
			this.setPostProcStruct(pipeline, outputIndex, spatializerNumber, synthArgs);
		};
	}

	makeAmbiPostProcName { |order = 1, spatializerNumber = 0|
		^("ambipost_" ++ "_s" ++ spatializerNumber ++ "_o" ++ order).asSymbol
	}

	replaceAmbiPostProcessor { |pipeline, order = 1, outputIndex = 0, spatializerNumber = 0, synthArgs = #[]|
		var bformatBus;

		if(statusWatcher.status !== \running) {
			"%: SATIE is not booted".format(thisMethod).warn
			^this
		};

		if(config.ambiOrders.includes(order)) {
			var index = config.ambiOrders.detectIndex { |item| item === order }; // stops on the first success
			bformatBus = config.ambiBus[index];
		} {
			Error("%: Invalid \'order\'. SATIE has only been configured for order(s) %".format(thisMethod, config.ambiOrders)).throw
		};

		fork {
			var postprocname = this.makeAmbiPostProcName(order, spatializerNumber);
			SynthDef(postprocname, { |synth_gate = 1|
				var xfade = EnvGen.kr(
					Env.asr(0.3, 1.0, 0.3, curve: \sine),
					gate: synth_gate,
					doneAction: Done.freeSelf
				);
				var previousSynth = In.ar(
					bus: bformatBus.index,
					numChannels: bformatBus.numChannels
				);
				// backing the hoa pipeline
				pipeline.do { |item|
					item = item.asSymbol;
					previousSynth = SynthDef.wrap(
						func: config.hoa.at(item).function,
						prependArgs: [previousSynth, order]
					);
					// add individual pipeline item to the dictionaries used by introspection
					groupInstances[\ambiPostProc].put(item, previousSynth);
					mastering.put(item, item);
				};
				previousSynth = previousSynth * xfade;
				Out.ar(outputIndex, previousSynth);
			}).add(synthDescLibName);
			config.server.sync;
			ambiPostProcessors[postprocname].set(\synth_gate, 0);
			ambiPostProcessors.put(postprocname, Synth(postprocname, args: synthArgs, target: ambiPostProcGroup));
			this.setAmbiPostProcStruct(pipeline, order, outputIndex, spatializerNumber, synthArgs);
		};
	}

	getAmbiPostProc{ |order = 1, spatializerNumber = 0|
		^ambiPostProcessors.at(this.makeAmbiPostProcName(order, spatializerNumber))
	}

	// private method
	makePostProcGroup {
		ambiPostProcGroup = ParGroup(config.server, \addToTail);
		groups.put(\ambiPostProc, ambiPostProcGroup);
		groupInstances.put(\ambiPostProc, Dictionary.new());
		postProcGroup = ParGroup(config.server, \addToTail);
		groups.put(\postProc, postProcGroup);
		groupInstances.put(\postProc, Dictionary.new());
	}

	setAuxBusses {
		postf("THIS IS THE SERVER OUTPUT BUS: %\n", config.server.outputBus);
		auxbus = Bus.audio(config.server, config.numAudioAux);
		postf("THIS IS THE SERVER AUX BUS: %\n", auxbus);

		aux = Array.fill(config.numAudioAux, {arg i; auxbus.index + i});
	}

	setAmbiBusses {
		config.ambiBus = Array.newClear(config.ambiOrders.size);
		config.ambiOrders.do { |order, index|
			config.ambiBus[index] = Bus.audio(config.server, (order + 1).pow(2).asInteger);
		};
	}

	loadBuffers {
		// load Binaural IRs if not loaded already
		if(HOABinaural.binauralIRs.isNil) {
			HOABinaural.loadbinauralIRs(config.server)
		};

		audioSamples.put(
			\lebedev50,
			SatieFactory.loadLebedev50Buffer(config.server)
		);
		config.server.sync;
	}


	loadSample { |name, path|

		if(config.server.serverRunning.not) {
			"%: Satie not booted. Cannot load sample '%'.".format(thisMethod, path).error;
			^this
		};

		if(name.class != Symbol) {
			"%: argument \'name\' must be a Symbol".format(thisMethod).error;
			^this
		};

		^Buffer.read(
			server: config.server,
			path: path.standardizePath,
			action: { |buf| audioSamples.put(name, buf) }
		)
	}

	setupPlugins {
		// execute setup functions in all plugins
		this.recurseExecSetup(this.getAllPlugins());
	}

	setupPlugin { |plug|
		if (plug.setup.notNil,
			{
				plug.setup.value(this);
			}
		);
	}

	recurseExecSetup { |plugs|
		// recurse SatiePlugins and exec .setup() on each available plugin
		plugs.do({ |plug|
			if (plug.isKindOf(SatiePlugins),
				{
					this.recurseExecSetup(plug);
				},
				{
					if (plug.isKindOf(SatiePlugin),
						{
							this.setupPlugin(plug);
						},
						{
							if(debug,
								{"% :: Tried setup on % but it is not a SatiePlugin, instead got %\n".format(this.class.getBackTrace, plug, plug.class).warn}
							);

						}
					);
				}
			);
		});
	}

	makePlugins {
		// generate synthdefs for source plugins
		config.generators.do { |item|
			if ((item.channelLayout == \mono).asBoolean,
				{
					this.makeSynthDef(item.name, item.name, [],[],[], config.listeningFormat, config.outBusIndex);
				});
			config.ambiOrders.do { |order, index|
				this.makeAmbi((item.name ++ "Ambi" ++ order).asSymbol, item.name, [], [], [], order, [], config.ambiBus[index]);
			};
		};

		// make processes
		config.processes.keysValuesDo { |name, env|
			this.makeProcess(name, env);
		};
	}

}
