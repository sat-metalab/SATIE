// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

+ Satie {

	findPlugin { |srcName|
		// looks for srcName in the relevant plugin dictionaries
		// this method returns the first plugin found
		// the return value is an array containing the SatiePlugin and it's type

		// if receiver is not Nil, evaluate function passing receiver as argument
		config.generators.at(srcName) !? { |plugin| ^[plugin, \source] };
		config.effects.at(srcName) !? { |plugin| ^[plugin, \effect] };

		Error("Could not find plugin named \'%\'".format(srcName)).throw
	}

	addPluginToDictionary { |type, name, srcName|
		switch(type)
		{ \source } { generators.add(name.asSymbol -> srcName.asSymbol) }
		{ \effect } { effects.add(name.asSymbol -> srcName.asSymbol) };
	}

	validateAmbiOrder { |ambiOrder|
		if(ambiOrder.isInteger.not) {
			Error("\'ambiOrder\' should be an Integer. Invalid value -> %%".format(ambiOrder.class, ambiOrder)).throw
		};
		if(config.ambiOrders.includes(ambiOrder).not) {
			Error("Invalid \'ambiOrder\'. SATIE has only been configured for order(s) %".format(config.ambiOrders)).throw
		}
	}

	validateAmbiBus { |order, bus|
		var bformatChans = (order + 1).pow(2).asInteger;
		if(bus.numChannels !== bformatChans) {
			Error("Invalid ambisonic Bus. Bus has % channels, should have % channels for order %.".format(bus.numChannels, bformatChans, order)).throw
		}
	}

	// if synthArgs contains a key value pair -> \bufnum, value
	// check whether value is a Symbol found in SATIE's audioSamples dictionary
	// if so, replace it with the buffer's bufnum
	processBufnum { |synthArgs|
		var bufnum, newBufnum, index;

		index = synthArgs.indexOf(\bufnum);
		if(index.isNil) { ^synthArgs };

		index = index + 1;
		bufnum = synthArgs[index];
		if(bufnum.class != Symbol) { ^synthArgs };

		newBufnum = audioSamples[bufnum];
		if(newBufnum.isNil) {
			Error("%: SATIE's audioSamples dictionary does not contain an audio sample named \'%\'".format(thisMethod, bufnum)).throw
		};

		^synthArgs.put(index, newBufnum.bufnum)
	}

	makeSynthDef {
		|
		name,
		srcName,
		srcPreToBusses,
		srcPostToBusses,
		srcPreMonitorFuncsArray,
		spatSymbolArray = #[],
		firstOutputIndexes = #[0],
		paramsMapper = \defaultMapper,
		synthArgs = #[]
		|

		var plugin, type;
		var src, preMonitorArray, spatializerArray;

		# plugin, type = this.findPlugin(srcName);

		if(plugin.channelLayout !== \mono) {
			"Plugin \'%\' has channelLayout \'%\'".format(plugin.name, plugin.channelLayout).warn;
			Error("Cannot call % for non \'\\mono\' plugins".format(thisMethod.name)).throw
		};

		if(spatSymbolArray.isEmpty) {
			// if spatSymbolArray is an empty array, return early
			if(debug) {
				"%: Empty spatializer array. Skipping SynthDef creation for name \'%\', srcName \'%\'".format(thisMethod.name, name, srcName).warn
			};
			^nil
		};

		spatSymbolArray.do { |item|
			var spatializer = config.spatializers.at(item);

			if(spatializer.isNil) {
				Error("%: Could not find spatializer named \'%\'".format(thisMethod.name, item)).throw;
			};

			if(spatializer.channelLayout !== \mono) {
				// warn user about incorrect channelLayout
				"Spatializer \'%\' has invalid channelLayout \'%\'".format(spatializer.name, spatializer.channelLayout).warn;
			}
		};

		src = plugin.function;
		spatializerArray = spatSymbolArray.collect { |item|
			config.spatializers.at(item).function;
		};
		preMonitorArray = srcPreMonitorFuncsArray.collect { |item|
			config.monitoring.at(item).function;
		};
		if(debug) { "params mapper %".format(paramsMapper).postln };
		paramsMapper = config.mappers.at(paramsMapper).function;

		SatieFactory.makeSynthDef(
			name,
			src,
			srcPreToBusses,
			srcPostToBusses,
			preMonitorArray,
			spatializerArray,
			firstOutputIndexes,
			paramsMapper,
			synthArgs
		);

		this.addPluginToDictionary(type, name, srcName);
	}

	makeAmbi {
		|
		name,
		srcName,
		preBusArray,
		postBusArray,
		srcPreMonitorFuncsArray,
		ambiOrder,
		ambiEffectPipeline = #[],
		ambiBus,
		paramsMapper = \defaultMapper,
		synthArgs = #[]
		|

		var plugin, type;
		var src, preMonitorArray;

		if(ambiBus.class !== Bus) {
			"%: Invalid ambiBus value. Expecting an audio Bus object, but got a %".format(thisMethod, ambiBus.class).error;
			^nil
		};

		# plugin, type = this.findPlugin(srcName);

		this.validateAmbiOrder(ambiOrder);
		this.validateAmbiBus(ambiOrder, ambiBus);

		src = plugin.function;
		preMonitorArray = srcPreMonitorFuncsArray.collect { |item|
			config.monitoring.at(item).function;
		};
		if(debug) { "params mapper %".format(paramsMapper).postln };
		paramsMapper = config.mappers.at(paramsMapper).function;

		if(plugin.channelLayout === \mono) {
			SatieFactory.makeAmbiFromMono(
				name,
				src,
				preBusArray,
				postBusArray,
				preMonitorArray,
				config.hoaEncoderType,
				ambiOrder,
				ambiEffectPipeline,
				ambiBus,
				paramsMapper,
				synthArgs
			)
		} {
			// else assume channelLayout is \ambi
			SatieFactory.makeAmbi(
				name,
				src,
				preBusArray,
				postBusArray,
				preMonitorArray,
				ambiOrder,
				ambiEffectPipeline,
				ambiBus,
				paramsMapper,
				synthArgs
			)
		};

		this.addPluginToDictionary(type, name, srcName);
	}

	makeAmbiFromMono {
		|
		name,
		srcName,
		preBusArray,
		postBusArray,
		srcPreMonitorFuncsArray,
		hoaEncoderType,
		ambiOrder,
		ambiEffectPipeline = #[],
		ambiBus,
		paramsMapper = \defaultMapper,
		synthArgs = #[]
		|

		var plugin, type;
		var src, preMonitorArray;

		if(ambiBus.class !== Bus) {
			"%: Invalid ambiBus value. Expecting an audio Bus object, but got a %".format(thisMethod, ambiBus.class).error;
			^nil
		};

		# plugin, type = this.findPlugin(srcName);

		if(plugin.channelLayout != \mono) {
			"Plugin \'%\' has channelLayout \'%\'".format(plugin.name, plugin.channelLayout).warn;
			Error("Cannot call % for non \'\\mono\' plugins".format(thisMethod.name)).throw
		};

		this.validateAmbiOrder(ambiOrder);
		this.validateAmbiBus(ambiOrder, ambiBus);

		src = plugin.function;
		preMonitorArray = srcPreMonitorFuncsArray.collect { |item|
			config.monitoring.at(item).function;
		};
		if(debug) { "params mapper %".format(paramsMapper).postln };
		paramsMapper = config.mappers.at(paramsMapper).function;
		// if hoaEncoderType not specified, use config.hoaEncoderType
		hoaEncoderType = hoaEncoderType ?? { config.hoaEncoderType };

		SatieFactory.makeAmbiFromMono(
			name,
			src,
			preBusArray,
			postBusArray,
			preMonitorArray,
			hoaEncoderType,
			ambiOrder,
			ambiEffectPipeline,
			ambiBus,
			paramsMapper,
			synthArgs
		);

		this.addPluginToDictionary(type, name, srcName);
	}

	makeInstance { |name, synthDefName, group = \default, synthArgs = #[]|
		var synth, nodeID;

		if(synthArgs.includes(\bufnum)) {
			synthArgs = this.processBufnum(synthArgs);
		};
		synth = Synth(synthDefName, args: synthArgs, target: groups[group], addAction: \addToHead);
		NodeWatcher.register(synth);
		if(groupInstances[group][name].notNil) {
			this.cleanInstance(name, group);
		};
		nodeID = synth.nodeID;
		namesIds.put(name, nodeID);
		groupInstances[group].put(name, synth);
		^synth
	}

	makeFxInstance{ |name, synthDefName, group = \defaultFx, synthArgs = #[]|
		this.makeSatieGroup(group.asSymbol, \addToEffects);
		^this.makeInstance(name, synthDefName, group, synthArgs)
	}

	makeSourceInstance{ |name, synthDefName, group = \default, synthArgs = #[]|
		this.makeSatieGroup(group.asSymbol);
		^this.makeInstance(name, synthDefName, group, synthArgs)
	}

	makeKamikaze { |synthDefName, group = \default, synthArgs = #[]|
		if(synthArgs.includes(\bufnum)) {
			synthArgs = this.processBufnum(synthArgs);
		};
		^Synth(synthDefName ++ "_kamikaze", args: synthArgs, target: groups[group], addAction: \addToHead)
	}

	makeSatieGroup { |name, addAction = \addToHead|
		var group;

		if(groups.includesKey(name.asSymbol).not) {
			if(addAction == \addToEffects) {
				group = ParGroup(groups[\defaultFx], \addAfter);
			} {
				group = ParGroup(config.server, addAction);
			};
			groups.put(name.asSymbol, group);
			groupInstances.put(name.asSymbol, Dictionary.new);
			^group
		};
	}

	killSatieGroup { |name|
		groups[name].free;
		groupInstances[name].free;
		groups.removeAt(name);
		groupInstances.removeAt(name);
	}

	cleanInstance { |name, group = \default|
		groupInstances[group][name].free;
		groupInstances[group].removeAt(name);
		namesIds.removeAt(name);
	}

	pauseInstance { |name, group = \default|
		groupInstances[group][name].release;
	}

	makeProcess { |processName, env|
		this.removeProcess(processName);
		"satieProcessManager: registering process environment: %".format(processName).postln;
		processes.put(processName.asSymbol, env);
		env[\satieInstance] = this;
		env.know = true;
		^env
	}

	removeProcess { |processName|
		if(processes.includesKey(processName.asSymbol)) {
			"un-registering process environment: %".format(processName).postln;
			// FIXME check if free is needed
			processes.removeAt(processName.asSymbol);
		};
	}

	cloneProcess { |processName|
		^processes.at(processName).copy
	}

	setProcessProperties { |process, arglist|
		// check for key \bufnum and process its value
		if(arglist.includes(\bufnum)) {
			arglist = this.processBufnum(arglist);
		};

		// set key values inside the process
		arglist.keysValuesDo { |key, val|
			process.put(key.asSymbol, val)
		};
	}

	// instantiate a process - also creates a unique group
	makeProcessInstance { |id, processName, arglist = #[], addAction = \addToHead|
		var groupName, myProcess;

		// empty arrays are size 0 and thus even
		if(arglist.size.odd) {
			"%: odd number of [key, value] arguments provided: %".format(thisMethod, arglist).warn;
			^nil
		};

		if(processes.includesKey(processName)) {
			myProcess = this.cloneProcess(processName.asSymbol);
		} {
			"%: undefined process environment: %".format(thisMethod, processName).warn;
			^nil
		};

		if(arglist.notEmpty) {
			this.setProcessProperties(myProcess, arglist);
		};

		groupName = (id ++ "_group").asSymbol;
		this.makeSatieGroup(groupName, addAction);
		processInstances.put(id, myProcess);
		myProcess.setup(id, groupName);
		^myProcess
	}

	cleanProcessInstance { |process|
		if(processInstances.includesKey(process)) {
			processInstances[process].cleanup;
			this.killSatieGroup((process ++ "_group").asSymbol);
			processInstances.removeAt(process);
		} {
			"%: A process named % was not found".format(thisMethod, process).warn;
		}
	}

	// deprecated class, use Satie.clearScene instead
	cleanUp {
		this.deprecated(thisMethod, Satie.findMethod(\clearScene));
	}

	cleanSlate {
		// keys.do creates a new Set[] containing the keys as Symbols
		// avoids iterating over a collection while simultaneously removing its contents

		// reset outputSynth
		if(hasOutputSynth) {
			outputSynth.set(\synth_gate, 0);
			outputSynth = nil;
		};

		// clear processes
		processInstances.keys.do { |process|
			this.cleanProcessInstance(process);
		};

		// flush all child nodes but keep the Group
		groups.keysValuesDo { |key, group|
			group.freeAll;
		};

		// clear namesIds
		this.namesIds.clear;

		// remove groups
		groups.keys.do { |name|
			this.killSatieGroup(name);
		};
	}
}
