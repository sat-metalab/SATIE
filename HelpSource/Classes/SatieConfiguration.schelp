TITLE:: SatieConfiguration
summary:: SATIE's configuration class
categories:: Libraries
related:: Overview/SATIE-Overview, Classes/Satie

DESCRIPTION::
This class is used to configure SATIE. It also provides access to its server's options. The initial configuration must be done prior to booting SATIE. The table below explains the order of setting of parameters:

SUBSECTION:: Configuration at init time
(strong::before:: SATIE)

table::
## LINK::#-server:: || configured via constructor
## LINK::#-listeningFormat:: || configured via constructor
## LINK::#-numAudioAux:: || configured via constructor
## LINK::#-outBusIndex:: || configured via constructor
## LINK::#-ambiOrders:: || configured via constructor
## LINK::#-minOutputBusChannels:: || configured via constructor
## LINK::#-userPluginsDir:: || configured via constructor
::

SUBSECTION:: Configuration of instance variables
(still strong::before:: SATIE)

table::
## LINK::#-hoaEncoderType:: || configured by user on the TELETYPE::SatieConfguration:: instance
## LINK::#-generateSynthdefs:: || configured by user on the TELETYPE::SatieConfguration:: instance
::

SUBSECTION:: Runtime
(before or after instantiating SATIE)

table::
## LINK::#-debug:: || configured by user on the TELETYPE::SatieConfguration:: instance
## LINK::#-orientationOffsetDeg:: || configured by user on the TELETYPE::SatieConfguration:: instance or via OSC
::


METHOD:: new
Create a new configuration.

ARGUMENT:: server
The server instance. The server should not be booted. SATIE can be used with either code::scsynth:: or code::supernova::.

ARGUMENT:: listeningFormat
An array of listening formats. The default is TELETYPE::[\stereoListener]::. Can be configured with one or more listening formats, each with their own TELETYPE::outBusIndex:: value. For a partial list of available formats, see LINK::#-handleSpatFormat::.

ARGUMENT:: numAudioAux
A number of auxiliary channels. Auxiliary channels are used for routing audio signals to effects, especially, but can have other uses.

ARGUMENT:: outBusIndex
An array of output channel offset values for the TELETYPE::listeningFormats::. There must be exactly one offset value per TELETYPE::listeningFormat::. The default is TELETYPE::[0]::, which configures a single TELETYPE::listeningFormat:: such that its signal is output starting from the server's first output channel. Overlapping or non-congruous channel setups are allowed (for the latter see TELETYPE::minOutputBusChannels:: below).

ARGUMENT:: ambiOrders
A list of Ambisonic orders. Currently, SATIE makes use of the SC-HOA quark and can be used up to order 3. You can specify multiple ambisonic orders in this list.

ARGUMENT:: minOutputBusChannels
The minimum number of server outputs channels. The default is TELETYPE::2::. This will ensure the server has at least this number of outputs. This can be used to guarantee an adequate number of output channels when doing non-congruous TELETYPE::outBusIndex:: setups.
NOTE::Upon initialization, the number of output busses/channels needed is calculated automatically based on TELETYPE::\listeningFormat::. The actual number of physical outputs will be the larger number between the calculated sum and this option.::
NOTE::The user must take into account the number of channels required for TELETYPE::\ambiOrders::. SATIE does not calculate this number automatically. If there are any TELETYPE::ambiOrders:: specified, the user must add that sum to any other channels.::

ARGUMENT:: userPluginsDir
An optional path to a user-provided directory of SATIE plugins. The default value is TELETYPE::""::, which is ignored. This feature is useful for loading project-specific plugins, such as spatializers,  prior to booting SATIE. This directory should be structured in the same way as described in LINK::Classes/SatiePlugin#-Description:: :

TELETYPE::plugins/::
TREE::
## TELETYPE::sources/::
	TREE::
	## TELETYPE::some_source_plugin.scd::
	::
## TELETYPE::effects/::
	TREE::
	## TELETYPE::some_effect_plugin.scd::
	## TELETYPE::etc.::
	::
## TELETYPE::hoa/::
## TELETYPE::mappers/::
## TELETYPE::monitoring/::
## TELETYPE::postprocessors/::
## TELETYPE::spatializers/::
::


returns:: A SatieConfiguration.

METHOD:: fromJsonFile
Create a new configuration from a JSON file

ARGUMENT:: path
A path to a JSON file containing a SATIE configuration. See LINK::#JSON Configuration#example below:: for a description of such a JSON configuration file.

returns:: A SatieConfiguration.

METHOD:: fromJsonString
Create a new configuration from stringified JSON

ARGUMENT:: string
Stringified JSON containing a SATIE configuration. See LINK::#JSON Configuration#example below:: for a description of such a JSON configuration file.

returns:: A SatieConfiguration.

INSTANCEMETHODS::

METHOD:: debug
Turns on debugging for this SatieConfiguration instance. Will post messages while SATIE boots.

METHOD:: server
Current server

returns:: Server instance

METHOD:: listeningFormat
An Array of Symbols corresponding to the spatializer plugin names that SATIE was configured to use.

returns:: Array

METHOD:: numAudioAux
see LINK::#new#Arguments::

returns:: Integer

METHOD:: outBusIndex
see LINK::#new#Arguments::

returns:: List

METHOD:: ambiOrders
see LINK::#new#Arguments::

returns:: List

METHOD:: minOutputBusChannels
see LINK::#new#Arguments::

returns:: Integer

METHOD:: userPluginsDir
see LINK::#new#Arguments::

returns:: String

METHOD:: spatBus
An Array of link::Classes/Bus#*audio:: objects corresponding to each of the configured teletype::listeningFormats::.
Spatializers output their signal to their bus, and the code::\satieOutput:: synth reads from these buses and routes their signals to the hardware outputs.

returns:: Array

METHOD:: ambiBus
An array of link::Classes/Bus#*audio:: objects corresponding to each of the configured teletype::#ambiOrders::.
Ambisonic synths output their order teletype::N:: B-format signal to the bus that corresponds to that order.
Ambisonic post-processors reads from these buses and route their signals to the hardware outputs.

returns:: Array

METHOD:: hoaEncoderType
Sets the type of Higher-order Ambisonics encoding to be used.

ARGUMENT:: type
A code::Symbol:: which is either code::\wave::, code::\harmonic::, or code::\lebedev50:: (default is code::\wave::)

DISCUSSION::
This setting has an effect on how SynthDefs for code::source:: type plugins are generated when using Ambisonics.
There are three possible choices: code::\wave::, code::\harmonic::, and code::\lebedev50::.
The default code::hoaEncoderType:: is code::\wave::. It will use the code::HOAEncoder:: UGen to encode a source into an Ambisonic B-format.
The encoder type called code::\harmonic:: makes use of code::HOASphericalHarmonics:: to get its source into B-format.
The code::\harmonic:: encoder is much more CPU efficient, whereas the code::\wave:: encoder is more configurable.
An alternative approach to encoding is used by the code::\lebedev50:: enocoder type.
It takes a mono source, encodes it into a VBAP 50 point Lebedev grid, and then encodes the signal into B-format using code::HOAEncLebedev50::.
This approach has the avantage of allowing the source to be panned using the code::VBAP:: UGen rather than the Ambisonic UGens from code::SC-HOA::.

METHOD:: generateSynthdefs
Boolean. Tells the SATIE server whether it should generate audio plugins SynthDefs automatically upon boot. True by default.
If set to false, the user needs to run LINK::Classes/Satie#-makeSynthDef:: by hand for each desired plugin.

METHOD:: loadPluginDir
Loads a SATIE plugin directory

argument:: path
A String representing the path to a plugin directory

discussion::
The plugin directory must be structured appropriately. See LINK::Classes/SatiePlugin::

METHOD:: sources
Instance variable holds audio plugins

returns:: Dictionary

METHOD:: effects
Instance variable for effects plugins.

returns:: Dictionary

METHOD:: spatializers
Instance variable hold spatializer plugins

returns:: Dictionary

METHOD:: mappers
Instance variable holds mapper plugins.

returns:: Dictionary

METHOD:: satieRoot
Absolute path to the root of SATIE's source code

returns:: String

METHOD:: satieUserSupportDir
Absolute path to SATIE's user support directory

table::
## Linux || ~/.local/share/satie/
## macOS || ~/Library/Application Support/satie/
## Windows || C:\Users\EMPHASIS::username::\AppData\Local\satie\
::
returns:: String

SECTION:: JSON Configuration

A strong::SatieConfiguration:: can be created from a JSON file that describes a configuration. This is done via the class method LINK::Classes/SatieConfiguration#*fromJsonFile::. An example JSON configuration is shown below.

code::
{
  "satie_configuration": {
    "server": {
      "name": null,
      "supernova": true
    },
    "listeningFormat": [ "stereoListener", "domeVBAP" ],
    "outBusIndex": [ 0, 2 ],
    "numAudioAux": 2,
    "ambiOrders": [ 1, 3 ],
    "minOutputBusChannels": 2,
    "userPluginsDir": nil
  }
}
::

The entire configuration is stored under the key called code::"satie_configuration"::. The nested JSON object that it contains has five keys: code::server::, code::listeningFormat::, code::outBusIndex::, code::numAudioAux::, code::ambiOrders::, and code::minOutputBusChannels::. The values that must be provided are listed below.

DEFINITIONLIST::
    ## server
    || value can be code::null:: or a JSON object code::{ }::
    ## server.name (optional)
    || value can be code::null:: or a String
    ## server.supernova (optional)
    || value can be code::null:: or a Boolean
    ## listeningFormat
    || value must be an Array of Strings
    ## outBusIndex
    || value must be an Array of Integers
    ## numAudioAux
    || value must be an Integer
    ## ambiOrders
    || value must be an Array of Integers
    ## minOutputBusChannels
    || value must be an Integer
::

See LINK::Classes/SatieConfiguration#*new:: for more information on the above configuration parameters.


SECTION:: Examples
code::
(
c = SatieConfiguration(
    server: s,
    listeningFormat: [\stereoListener],
    ambiOrders: [3]
);
c.hoaEncoderType = \harmonic;
a = Satie(c);
)

a.boot;
a.quit;
::
